/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daniel
 */
public class Registro {
    private int numServicio;
    private String fecha;
    private int tipoServicio;
    private VehiculoCarga vehiculo;
    private String descripcionAutomovil;
    private float costoBase;
    
    public Registro(){
        this.numServicio = 0;
        this.fecha = "";
        this.tipoServicio = 0;
        this.descripcionAutomovil = "";
        this.costoBase = 0.0f;
        this.vehiculo = new VehiculoCarga();
    }

    public Registro(int numServicio, String fecha, int tipoServicio, VehiculoCarga vehiculo, String descripcionAutomovil, float costoBase) {
        this.numServicio = numServicio;
        this.fecha = fecha;
        this.tipoServicio = tipoServicio;
        this.vehiculo = vehiculo;
        this.descripcionAutomovil = descripcionAutomovil;
        this.costoBase = costoBase;
    }
    
    public Registro (Registro registro){
        this.numServicio = registro.numServicio;
        this.fecha = registro.fecha;
        this.tipoServicio = registro.tipoServicio;
        this.vehiculo = registro.vehiculo;
        this.descripcionAutomovil = registro.descripcionAutomovil;
        this.costoBase = registro.costoBase;
    }

    public int getNumServicio() {
        return numServicio;
    }
    
    public void setNumServicio(int numServicio) {
        this.numServicio = numServicio;
    }
    
    public String getFecha() {
        return fecha;
    }
    
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    public int getTipoServicio() {
        return tipoServicio;
    }
    
    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }
    
    public VehiculoCarga getVehiculo() {
        return vehiculo;
    }
    
    public void setVehiculo(VehiculoCarga vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    public String getDescripcionAutomovil() {
        return descripcionAutomovil;
    }
    
    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }
    
    public float getCostoBase() {
        return costoBase;
    }
    
    public void setCostoBase(float costoBase) {
        this.costoBase = costoBase;
    }
    
    public float costoServicio(int tipoMotor) {
        float incremento = 0;
        switch (tipoMotor){
            case 1:
                incremento = this.costoBase * 0.25f;
                break;
            case 2:
                incremento = this.costoBase * 0.50f;
                break;
            case 3:
                incremento = this.costoBase * 1.50f;
                break;
            case 4:
                incremento = this.costoBase * 2.0f;
                break;
            default:
                System.out.println("Tipo de motor no válido");
                return this.costoBase;   
        }
        return this.costoBase + incremento;   
    }
    
    public float totalImpuesto(float tipoMotor) {
        float costoServicio = this.costoServicio((int) tipoMotor);   
        return costoServicio * 0.16f;
    }
    
    public float calcularTotal(int tipoMotor){
        float costoServicio = this.costoServicio(tipoMotor);
        float totalImpuesto = this.totalImpuesto(tipoMotor);
        return costoServicio + totalImpuesto;
    }
}
