/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daniel
 */
public class VehiculoCarga extends Vehiculos {
    
    private float numToneladas;
    
    public VehiculoCarga(){
        this.numToneladas = 0;
    }


    
    public VehiculoCarga(VehiculoCarga VehiCarga) {
        this.numToneladas = VehiCarga.numToneladas;
    }
    

    public VehiculoCarga(float numToneladas, int serieVehiculo, int tipoMotor, String marca, String modelo) {
        super(serieVehiculo, tipoMotor, marca, modelo);
        this.numToneladas = numToneladas;
    }

    
    public float getNumToneladas() {
        return numToneladas;
    }

    public void setNumToneladas(float numToneladas) {
        this.numToneladas = numToneladas;
    }
}