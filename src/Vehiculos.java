/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daniel
 */
public abstract class Vehiculos {
    protected int serieVehiculo;
    protected int tipoMotor;
    protected String marca;
    protected String modelo;
    
    public Vehiculos(){
        this.serieVehiculo = 0;
        this.marca = "";
        this.modelo = "";
        this.tipoMotor = 0;
    }
    public Vehiculos(Vehiculos vehi){
        this.serieVehiculo =  vehi.serieVehiculo;
        this.marca = vehi.marca;
        this.modelo = vehi.modelo;
        this.tipoMotor = vehi.tipoMotor;
    }

    public Vehiculos(int serieVehiculo, int tipoMotor, String marca, String modelo) {
        this.serieVehiculo = serieVehiculo;
        this.tipoMotor = tipoMotor;
        this.marca = marca;
        this.modelo = modelo;
    }

 

    public int getSerieVehiculo() {
        return serieVehiculo;
    }

    public void setSerieVehiculo(int serieVehiculo) {
        this.serieVehiculo = serieVehiculo;
    }

    public int getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(int tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    
    
    
    
}
